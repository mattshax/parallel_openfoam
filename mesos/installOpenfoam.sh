apt-get -q update && \
    apt-get -q -y upgrade && \
    apt-get -q -y install adduser vim zip unzip imagemagick libav-tools python3 python3-dev && \
    curl -s https://bootstrap.pypa.io/get-pip.py | python3 && \
    apt-get -q -y install libfreetype6-dev libpng12-dev libatlas-base-dev gfortran && \
    apt-get -q -y install libtiff4-dev libjpeg8-dev zlib1g-dev liblcms2-dev libwebp-dev && \
    echo "deb http://www.openfoam.org/download/ubuntu trusty main" > /etc/apt/sources.list.d/openfoam.list && \
    apt-get -q update && \
    apt-get -q -y --force-yes install openfoam231
    
    # place this at TOP of bashrc file
echo ". /opt/openfoam231/etc/bashrc" >> ~/.bashrc;sudo su

# enable ssh without password
#rm ~/.ssh/authorized_hosts;cat /gluster/OPENFOAM_PARALLEL/id_rsa.pub > ~/.ssh/authorized_keys
#chmod 700 ~/.ssh;chmod 600 ~/.ssh/authorized_keys