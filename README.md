# Urban CFD Analysis using Parallel OpenFOAM

This repository details a sample district/urban-scale OpenFOAM model running in parallel. To view information about this demonstration urban OpenFOAM run, please refer to blockgenerator/interactiveblockgenerator.pdf, or for video demonstrations blockgenerator/interactiveblockgenerator.pptx

* * *

To start the OpenFOAM demonstration on a multicore host resource, run the commands below. Please note the scripts may not work completely seamlessly depending on runtime environment.


Clone the repository
```
git clone git@bitbucket.org:mattshax/parallel_openfoam.git
```

Install OpenFOAM
```
cd parallel_openfoam
mesos/installOpenfoam.sh
```

Run the OpenFOAM demonstration using the script below

```
cd sampleUrbanRun
./runParallel.sh
```

Additionally, scripts are included (and commented) in the runParallel.sh file, cluster and mesos directories to enable cross-host MPI runs with docker containers.
