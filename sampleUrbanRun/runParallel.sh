#!/bin/bash
echo "******************"
echo "*    Run CFD     *"
echo "******************"

hostfile=hostFileLocal
np=12

blockMesh -dict system/blockMeshDict 
decomposePar -force -noFunctionObjects

## run snappyHexMesh in parallel on host resource
mpirun -np $np snappyHexMesh -overwrite -parallel

## run snappyHexMesh in parallel across hostFile resources
#mpirun -np $np --mca btl_tcp_if_include eth0 -hostfile $hostfile snappyHexMesh -overwrite -parallel

reconstructParMesh -constant
rm processor* -R
cp 0.org 0 -R

decomposePar

## run renumberMesh in parallel on host resource
mpirun -np $np renumberMesh -overwrite -parallel

## run renumberMesh in parallel across hostFile resources
#mpirun -np $np --mca btl_tcp_if_include eth0 -hostfile $hostfile renumberMesh -overwrite -parallel

## run simpleFoam in parallel on host resource
mpirun -np $np simpleFoam -parallel > simpleFoam.log &

## run simpleFoam in parallel across hostFile resources
#mpirun -np $np --mca btl_tcp_if_include eth0 -hostfile $hostfile simpleFoam -parallel > simpleFoam.log &

pid=$!
while kill -0 "$pid" 2>/dev/null; do
  tail -n 10 simpleFoam.log
  gnuplot ResidualGraphs
  sleep 1 
done
gnuplot ResidualGraphs

reconstructPar -latestTime

rm processor* 0 *.log -R
foamToVTK -ascii -latestTime


# head node
  # docker run --name headnode -h compute -it -p 3050:3050 -v /HOST_LOCATION:/core/input_files mattshax/openfoam_headnode sh -c "service ssh start;service c9 start;bin/bash"

# worker node
  # docker run --name worker1 -h compute -v /HOST_LOCATION:/core/input_files -d mattshax/openfoam_headnode sh -c "/usr/sbin/sshd -D" 
  # docker inspect --format '{{ .NetworkSettings.IPAddress }}' worker1
