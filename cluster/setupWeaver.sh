# DOCKER WEAVING (LET ALL DOCKER CONTAINERS TALK TO EACH OTHER)

# MAKE SURE LATEST DOCKER INSTALLED
apt-get install lxc-docker

# ON EVERY HOST PROVIDE UNIQUE SUBNET VALUE
sudo wget -O /usr/local/bin/weave https://github.com/zettio/weave/releases/download/latest_release/weave;sudo chmod a+x /usr/local/bin/weave;
nano /etc/network/interfaces
    auto weave
    iface weave inet manual
            pre-up /usr/local/bin/weave create-bridge
            post-up ip addr add dev weave 10.2.0.1/16
            pre-down ifconfig weave down
            post-down brctl delbr weave
ifup weave

nano /etc/default/docker
    DOCKER_OPTS="--bridge=weave --fixed-cidr=10.2.1.0/24"
    DOCKER_OPTS="--bridge=weave --fixed-cidr=10.2.2.0/24"
    DOCKER_OPTS="--bridge=weave --fixed-cidr=10.2.3.0/24"
    ...
service docker restart

# ON HEANODE
weave launch

# ON EACH WORKER
weave launch 172.20.24.20 172.20.24.102 172.20.24.104 172.20.24.106 172.20.24.107 172.20.24.108 172.20.24.109

# TESTING THE COMMUNICATION
docker run -it mattshax/openfoam_headnode /bin/bash
