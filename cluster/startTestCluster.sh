#! /bin/bash
# this script runs an MPI test from worker nodes, and a head node in which the MPI script is executed

echo ""

# start the worker nodes and get each IP Address
echo "Starting worker nodes..."
worker1_name="worker1"
worker1=$(docker run --name $worker1_name -h compute -d mattshax/openfoam_headnode sh -c "/usr/sbin/sshd -D")
worker1_IP=$(docker inspect --format '{{ .NetworkSettings.IPAddress }}' $worker1)
echo ""

# start the headnode and run the sample MPI script to test connection
echo "Running MPI Test..."
docker run -h compute -it mattshax/openfoam_headnode sh -c "mpirun -H $worker1_IP -np 4 ~/mpi_hello_world -parallel"
echo "MPI Test Complete..."

echo ""
echo "Stopping Worker Nodes..."
# stop and remove worker and head nodes
docker stop $worker1_name > /dev/null
docker rm $worker1_name > /dev/null
# clean up - remove all unused containers
#docker rm $(docker ps -a -q)

echo ""
echo "All Done!"


# when starting docker container, mount in the input files to headnode and workers to same location


