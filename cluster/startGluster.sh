# glusterfs install procedure
sudo apt-get install python-software-properties -y;sudo apt-get install software-properties-common -y;sudo add-apt-repository ppa:gluster/glusterfs-3.5;sudo apt-get update;sudo apt-get install glusterfs-server -y;mkdir -p /mnt/gluster

nano /etc/hosts
172.20.24.20    cluster20
172.20.24.102   cluster102
172.20.24.103   cluster103
172.20.24.104   cluster104
172.20.24.106   cluster106
172.20.24.107   cluster107
172.20.24.108   cluster108
172.20.24.109   cluster109
172.20.24.101   cluster101
172.20.24.105   cluster105

gluster peer probe cluster102 # for all addresses

# to create volume
gluster  volume create datapoint replica 2 transport tcp  cluster20:/mnt/gluster  cluster103:/mnt/gluster force
gluster volume start datapoint

# to add brick
gluster volume add-brick datapoint cluster102:/mnt/gluster cluster104:/mnt/gluster force
mkdir /gluster;mount -t glusterfs cluster20:/datapoint /gluster;
